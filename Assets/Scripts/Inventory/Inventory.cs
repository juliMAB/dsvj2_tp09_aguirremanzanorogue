using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	#region Singleton

	public static Inventory instance;

	void Awake ()
	{
		instance = this;
	}

	#endregion

	public int space = 20; 
	public int playerSpace = 6;

    [SerializeField] private GameObject player;
    [SerializeField] private GameObject inventory;

    public InventorySlot[] slotsPlayer;
    public InventorySlot[] slotsInventory;

    public List<Item> items = new List<Item>();
	public List<Item> playerItems = new List<Item>();

    void OnDestroy()
    {
        //SaveInventory();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            SaveSystem.instance.SaveInventory(this);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            Inventory.instance.LoadInventory();
        }
    }

    void Start()
    {
        slotsPlayer = player.GetComponentsInChildren<InventorySlot>();
        slotsInventory = inventory.GetComponentsInChildren<InventorySlot>();
        //LoadInventory();
    }

    bool GetFreeInventorySlot(out InventorySlot inventorySlot)
    {
        foreach (InventorySlot iS in slotsInventory)
        {
            if (!iS.HasAnItem)
            {
                inventorySlot = iS;
                return true;
            }
        }

        inventorySlot = null;
        return false;
    }

    bool GetFreePlayerInventorySlot(out InventorySlot inventorySlot, Item item)
    {
        foreach (InventorySlot inventoryslot in slotsPlayer)
        {
            if (!inventoryslot.HasAnItem)
            { 
                if (inventoryslot.gameObject.CompareTag("Slot" + (int)item.conteinerSlot))
                {
                    inventorySlot = inventoryslot;
                    return true;
                }
            }
        }

        inventorySlot = null;
        return false;
    }

    public void Move(InventorySlot iSlot)
    {
        if (!iSlot.item)
            return;

        if (iSlot.item.Equiped)
        {
            InventorySlot inventorySlot;

            if (GetFreeInventorySlot(out inventorySlot))
            {
                inventorySlot.AddItem(iSlot.item);
                inventorySlot.item.Equiped = false;
                iSlot.EmptySlot();
            }
		}
        else
        {
            InventorySlot inventorySlot;

            if (GetFreePlayerInventorySlot(out inventorySlot, iSlot.item))
            {
                inventorySlot.AddItem(iSlot.item);
                inventorySlot.item.Equiped = true;
                iSlot.EmptySlot();
            }
		}
    }

    public void Add (Item item)
	{
		if (items.Count >= space) {
			return;
		}

		items.Add (item);
	}

    public void AddToEquiped(Item item)
    {
        if (playerItems.Count >= playerSpace)
        {
            return;
        }

        playerItems.Add(item);
	}

	public void Remove (Item item)
	{
		items.Remove(item);

	}

    public void SaveInventory()
    {
		SaveSystem.instance.SaveInventory(this);
    }

    public void LoadInventory()
    {
        InventoryData data = SaveSystem.instance.LoadInventory();
        items.Clear();
		playerItems.Clear();
        Item a = new Item();
        for (int i = 0; i < data.cuantityItemsInventory+data.cuantityItemsPlayer; i++)
        {
            a.name = data.name[i];
            a.type = data.type[i];
            a.subtype = data.subtype[i];
            a.weight = data.weight[i];
            a.durability = data.durability[i];
            a.level = data.level[i];
            a.Equiped = data.equiped[i];
            if (i < data.cuantityItemsInventory)
            {
                items.Add(a);
			}
            else
            {
				playerItems.Add(a);
			}
        }
    }
}

public enum ConteinerSlot { Head = 1, Chest, Hand, Feet, Bag}
