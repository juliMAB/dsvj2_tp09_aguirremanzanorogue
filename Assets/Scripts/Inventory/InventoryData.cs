﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryData
{
    public int cuantityItemsInventory;
    public int cuantityItemsPlayer;
    public string[] name;
    public string[] type;
    public string[] subtype;
    public float[] weight;
    public int[] durability;
    public int[] level;
    public bool[] equiped;
    public int[] itemSlotId;

    public int[] slotId;
    public int[] singleID;
    public bool[] HasAnItem;

    public InventoryData(Inventory inventory)
    {
        cuantityItemsInventory = inventory.items.Count;
        cuantityItemsPlayer = inventory.playerItems.Count;
        int maxSpaces = cuantityItemsInventory + cuantityItemsPlayer;
        name = new string[maxSpaces];
        type = new string[maxSpaces];
        subtype = new string[maxSpaces];
        weight = new float[maxSpaces];
        durability = new int[maxSpaces];
        level = new int[maxSpaces];
        equiped= new bool[maxSpaces];
        singleID= new int[maxSpaces];
        HasAnItem = new bool[maxSpaces];
        itemSlotId = new int[maxSpaces];
        slotId = new int[maxSpaces];
        for (int i = 0; i < maxSpaces; i++)
        {
            if (i< inventory.items.Count)
            {
                name[i] = inventory.items[i].name;
                type[i] = inventory.items[i].type;
                subtype[i] = inventory.items[i].subtype;
                weight[i] = inventory.items[i].weight;
                durability[i] = inventory.items[i].durability;
                level[i] = inventory.items[i].level;
                equiped[i] = inventory.items[i].Equiped;
                itemSlotId[i] = inventory.items[i].GetInstanceID();
            }
            else
            {
                name[i] = inventory.playerItems[maxSpaces - i].name;
                type[i] = inventory.playerItems[maxSpaces - i].type;
                subtype[i] = inventory.playerItems[maxSpaces - i].subtype;
                weight[i] = inventory.playerItems[maxSpaces - i].weight;
                durability[i] = inventory.playerItems[maxSpaces - i].durability;
                level[i] = inventory.playerItems[maxSpaces - i].level;
                equiped[i] = inventory.playerItems[maxSpaces - i].Equiped;
                itemSlotId[i] = inventory.items[maxSpaces - i].GetInstanceID();
            }
        }

        for (int i = 0; i < inventory.slotsInventory.Length; i++)
        {
            if (inventory.slotsInventory[i].item!=null)
            {
                slotId[i] = inventory.slotsInventory[i].item.GetInstanceID();
            }
            singleID[i] = inventory.slotsInventory[i].singleID;
            HasAnItem[i] = inventory.slotsInventory[i].HasAnItem;
        }

        for (int i = inventory.slotsInventory.Length; i < inventory.slotsInventory.Length + inventory.slotsPlayer.Length; i++)
        {
            if (inventory.slotsInventory[i].item != null)
            {
                slotId[i] = inventory.slotsInventory[i].item.GetInstanceID();
            }
            singleID[i] = inventory.slotsInventory[i].singleID;
            HasAnItem[i] = inventory.slotsInventory[i].HasAnItem;
        }
    }
}
