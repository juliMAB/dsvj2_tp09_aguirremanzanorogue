using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour {

	public static int id = 0;
	public int singleID;
	public Image icon;
	public Button removeButton;
	public bool HasAnItem=false;
	public Item item;  

	public RectTransform panel;

    private void Start()
    {
		singleID = id;
		id++;
    }

    public void AddItem (Item newItem)
	{
		item = newItem;

		icon.sprite = item.icon;
		icon.enabled = true;
		HasAnItem = true;
		removeButton.interactable = true;
	}

	public void RemoveItemFromInventory ()
	{
		Inventory.instance.Remove(item);
		EmptySlot();
	}

	public void EmptySlot()
	{
		item = null;

		icon.sprite = null;
		icon.enabled = false;
		HasAnItem = false;
		removeButton.interactable = false;
	}

	public void UseItem ()
	{
		if (item != null)
		{
			item.Use();
		}
	}

	public void ShowInfo()
    {
        if (item)
        {
			panel.gameObject.SetActive(true);
            panel.gameObject.GetComponentInChildren<Text>().text =
                "Name: " + item.name + "\n" +
                "Type: " + item.type + "\n" +
                "Subtype: " + item.subtype + "\n" +
                "Weight: " + item.weight + "\n" +
                "Durability: " + item.durability + "\n" +
                "Level: " + item.level;
		}
    }

	public void CloseInfoPanel()
	{
		panel.gameObject.SetActive(false);
	}
}
