﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    public ItemList itemlist;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            foreach (var inventoryslot in GameObject.FindGameObjectsWithTag("InventorySlot"))
            {
                InventorySlot inventorySlot = inventoryslot.GetComponent<InventorySlot>();

                if (!inventorySlot.HasAnItem)
                {
                    int randomItem = Random.Range(0, itemlist.items.Length);
                    inventorySlot.HasAnItem = true;
                    int aux = Random.Range(0, 3);
                    
                    Inventory.instance.Add(itemlist.items[randomItem]);
                    inventorySlot.AddItem(itemlist.items[randomItem]);

                    return;
                }
            }
        }
    }

}
