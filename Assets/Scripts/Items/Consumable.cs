using UnityEngine;

public class Consumable : Item {

    public int healthGain;
    public ConsumableSubtype cst;

	public override void Use()
	{
		//eat
		//RemoveFromInventory();
	}

}

public enum ConsumableSubtype { Potion, Food, Last }
