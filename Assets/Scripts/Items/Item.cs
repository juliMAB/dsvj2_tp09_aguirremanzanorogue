using UnityEngine;

public class Item : ScriptableObject {

	new public string name = "New Item";    
	public string type = "Type";    
	public string subtype = "Subtype";    
	public float weight = 0;	// kg
	public int durability = 0;	//hits
	public int level = 0;
	public Sprite icon = null;				
	public bool Equiped = false;
	public TypeOfItem typeOfItem;
	public ConteinerSlot conteinerSlot;

	public virtual void Use ()
	{
		// Usar item
	}

	public void RemoveFromInventory ()
	{
		Inventory.instance.Remove(this);
	}

}

public enum TypeOfItem { Armor, Equipment, Consumable}
