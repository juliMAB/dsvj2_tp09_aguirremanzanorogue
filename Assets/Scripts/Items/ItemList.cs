﻿using UnityEngine;

[CreateAssetMenu(fileName = "New ItemList", menuName = "Inventory/ItemList")]
public class ItemList : ScriptableObject
{
    public Item[] items;
}