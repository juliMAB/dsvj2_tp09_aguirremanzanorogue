﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
[ExecuteInEditMode]
[AddComponentMenu("Event/RightClickEvent")]
public class RightClickEvent : MonoBehaviour
{
    [System.Serializable] public class RightButton : UnityEvent { }
    public RightButton onRightDown;
    public RightButton onRightUp;
    private bool isOver = false;

    void Update()
    {
        if (Input.GetMouseButtonDown(1) && isOver)
        {
            onRightDown.Invoke();
            print("Tocaste el boton");
        }
        if (Input.GetMouseButtonUp(1))
        {
            onRightUp.Invoke();
        }
    }

    public void OnPointerEnter()
    {
        isOver = true;
    }

    public void OnPointerExit()
    {
        isOver = false;
    }
}
